from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
      "import_href", "closet_name", "bin_number", "bin_size"
    ]

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


@require_http_methods(["GET", "POST"])
def api_shoes_list(request):
    if request.method == "GET":
      shoes = Shoe.objects.all()
      return JsonResponse(
        shoes,
        encoder=ShoeEncoder,
        safe=False
      )
    else: #POST
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin href"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
          shoe,
          encoder=ShoeEncoder,
          safe=False
        )

@require_http_methods(["DELETE"])
def api_delete_shoes(request, pk):
    try:
        shoe = Shoe.objects.get(id=pk)
        shoe.delete()
        return JsonResponse(
          shoe,
          encoder=ShoeEncoder,
          safe=False,
        )
    except Shoe.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})

