from django.urls import path
from .views import api_shoes_list, api_delete_shoes


urlpatterns = [
    path('shoes/', api_shoes_list, name="api_shoes"),
    path('shoes/<int:pk>/', api_delete_shoes, name="delete_shoes"),
]
