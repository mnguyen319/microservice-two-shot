from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(default=1)
    shelf_number = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
      return self.closet_name.title() + " " + self.import_href[-2:-1]

class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=15)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )

    def __str__(self):
      return self.style + " " + self.fabric + " " + str(self.id)