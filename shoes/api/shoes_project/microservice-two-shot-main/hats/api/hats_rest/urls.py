from django.urls import path
from .views import api_hats_list, api_delete_hat


urlpatterns = [
    path('hats/', api_hats_list, name="api_hats"),
    path('hats/<int:pk>/', api_delete_hat, name="delete_hat"),
]
