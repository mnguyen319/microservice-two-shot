from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
      "import_href", "closet_name"
    ]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def api_hats_list(request):
    if request.method == "GET":
      hats = Hat.objects.all()
      return JsonResponse(
        hats,
        encoder=HatEncoder,
        safe=False
      )
    else: #POST
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
          hat,
          encoder=HatEncoder,
          safe=False
        )

@require_http_methods(["DELETE"])
def api_delete_hat(request, pk):
    try:
        hat = Hat.objects.get(id=pk)
        hat.delete()
        return JsonResponse(
          hat,
          encoder=HatEncoder,
          safe=False,
        )
    except Hat.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})

