import React from 'react';

class ShoeDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shoes: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleShoeSelection = this.handleShoeSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ shoes: data });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const locationUrl = `http://localhost:8080/api/shoes/${this.state.shoe}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe)

      // Remove shoe from state without making new API call
      let remainingShoes = []
      for (let i = 0; i < this.state.shoes.length; i++) {
        let currentShoe = this.state.shoes[i]
        if (parseInt(this.state.shoe) !== currentShoe.id) {
          remainingShoes.push(currentShoe)
        }
      }

      this.setState({
        shoe: '',
        shoes: remainingShoes
      })
    }
  }

  handleShoeSelection(event) {
    const value = event.target.value;
    this.setState({ shoe: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Shoes To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <select onChange={this.handleShoeSelection} value={this.state.shoe} required name="shoe" id="shoe" className="form-select">
                  <option value="">Choose shoes</option>
                  {this.state.shoes.map(shoe => {
                    return (
                      <option key={shoe.id} value={shoe.id}>{shoe.model_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeDelete;
