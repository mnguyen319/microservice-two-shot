import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import HatDelete from './HatDelete';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import ShoeDelete from './ShoeDelete';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={props.hats} />}/>
            <Route path="new" element={<HatForm />}/>
            <Route path="delete" element={<HatDelete />}/>
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList shoes={props.shoes} />}/>
            <Route path="new" element={<ShoeForm />}/>
            <Route path="delete" element={<ShoeDelete />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
