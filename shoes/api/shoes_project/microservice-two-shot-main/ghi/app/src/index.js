import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));


async function loadWardrobe() {
  let hatData, shoeData;
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatResponse = await fetch('http://localhost:8090/api/hats/');

  if (shoeResponse.ok) {
    shoeData = await shoeResponse.json();
    console.log('shoe data: ', shoeData)
  } else {
    console.error(shoeResponse);
  }
  if (hatResponse.ok) {
    hatData = await hatResponse.json();
    console.log('hat data: ', hatData)
  } else {
    console.error(hatResponse);
  }

  root.render(
    <React.StrictMode>
      <App shoes={shoeData} hats={hatData} />
    </React.StrictMode>
  );
}
loadWardrobe();