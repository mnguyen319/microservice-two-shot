function ShoeList(props) {
  return (
    <div className="container">
      <h2 className="display-5 fw-bold">Shoes</h2>
      <div className="row">
        {props.shoes.map(shoe => {
          return (
            <div key={shoe.id} className="col">
              <div className="card mb-3 shadow">
                <div className="card-body">
                  <h5 className="card-title">{shoe.model_name}</h5>
                  <h6 className="card-subtitle mb-2 text-muted">
                    {shoe.manufacturer}
                  </h6>
                  <p className="card-text">
                    {shoe.bin}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default ShoeList