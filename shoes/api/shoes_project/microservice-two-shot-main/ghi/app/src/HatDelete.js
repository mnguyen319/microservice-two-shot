import React from 'react';

class HatDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hats: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleHatSelection = this.handleHatSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ hats: data });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const locationUrl = `http://localhost:8090/api/hats/${this.state.hat}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat)

      // Remove hat from state without making new API call
      let remainingHats = []
      for (let i = 0; i < this.state.hats.length; i++) {
        let currentHat = this.state.hats[i]
        if (parseInt(this.state.hat) !== currentHat.id) {
          remainingHats.push(currentHat)
        }
      }

      this.setState({
        hat: '',
        hats: remainingHats
      })
    }
  }

  handleHatSelection(event) {
    const value = event.target.value;
    this.setState({ hat: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Hat To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <select onChange={this.handleHatSelection} value={this.state.hat} required name="hat" id="hat" className="form-select">
                  <option value="">Choose a hat</option>
                  {this.state.hats.map(hat => {
                    return (
                      <option key={hat.id} value={hat.id}>{hat.style}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatDelete;
