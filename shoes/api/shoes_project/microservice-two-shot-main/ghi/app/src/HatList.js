function HatList(props) {
  return (
    <div className="container">
      <h2 className="display-5 fw-bold">Hats</h2>
      <div className="row">
        {props.hats.map(hat => {
          return (
            <div key={hat.id} className="col">
              <div className="card mb-3 shadow">
                <div className="card-body">
                  <h5 className="card-title">{hat.style}</h5>
                  <h6 className="card-subtitle mb-2 text-muted">
                    {hat.fabric}
                  </h6>
                  <p className="card-text">
                    {hat.location}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default HatList