from django.contrib import admin

# Register your models here.


from .models import BinVO, Shoes

@admin.register(Shoes)
class ShoeAdmin(admin.ModelAdmin):
    pass


@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    pass