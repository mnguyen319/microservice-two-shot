# Generated by Django 4.0.3 on 2022-05-04 23:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shoes',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
