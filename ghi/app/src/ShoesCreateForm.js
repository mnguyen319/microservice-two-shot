import React from "react";

class ShoesCreateForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model_name: '',
            color: '',
            manufactor: '',
            bin:'',
            bins: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleManufactorChange = this.handleManufactorChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;
        //console.log(data);
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoes = await response.json();
            console.log(newShoes);
            const cleared = {
                model_name: '',
                color: '',
                manufactor: '',
                bin: '',
              };
              this.setState(cleared);
        }
    };

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({model_name: value});
    };

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value});
    };

    handleManufactorChange(event) {
        const value = event.target.value;
        this.setState({manufactor: value});
    };

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value});
    };

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.bins});
            //console.log(data.bins)
            };
        };


    render() {
        return(
            <div className="my-5 container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new pair of shoes</h1>
                            <form onSubmit={this.handleSubmit} id="create-location-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} value={this.state.model_name} placeholder="model_name" required type="text"  name="model_name" id="model_name" className="form-control"/>
                                    <label htmlFor="model_name">Model name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                                    <label htmlFor="color">Color</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleManufactorChange} value={this.state.manufactor} placeholder="Manufactor" required type="text" name="manufactor" id="manufactor" className="form-control"/>
                                    <label htmlFor="manufactor">Manufactor</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.href}>{bin.closet_name} / {bin.bin_number}</option>
                                        );
                                    })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};

export default ShoesCreateForm;
