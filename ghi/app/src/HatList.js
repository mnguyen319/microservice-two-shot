import React from "react";
import { Link, NavLink } from 'react-router-dom';


function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(hat => {
        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img
              src={hat.picture_url}
              className="card-img-top"
            />
            <div className="card-body">
              <h5 className="card-title">{hat.style}</h5>
              <ul>
                <li className="card-text">Fabric: {hat.fabric}</li>
                <li className="card-text">Color: {hat.color}</li>
              </ul>
            </div>
            <div className="card-footer">
              <h5 className="card-title">Closet Name:</h5>
              <p className="card-text">{hat.location.closet_name}</p>
              <Link
                to="/hats/update"
                className="btn btn-primary btn-lg px-4 gap-3"
              >
                Update
              </Link>
              
            </div>
          </div>
        );
      })}
    </div>
  );
}

class HatList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hatColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8090/api/hats/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of conferences
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the conference
        // information into
        const hatColumns = [[], [], []];

        // Loop over the conference detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            hatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        // Set the state to the new list of three lists of
        // conferences
        this.setState({ hatColumns: hatColumns });
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>

        <div className="container">
          <h2>Hats List</h2>
          <div className="row">

            {this.state.hatColumns.map((hatList, index) => {
              return <HatColumn key={index} list={hatList} />;
            })}
          </div>
        </div>
      </>
    );
  }
}

export default HatList;
