import React from "react";

class HatUpdateForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            style: '',
            fabric: '',
            color: '',
            location: '',
            locations: []

        };
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // data.location = data.location;
        // delete data.location;
        delete data.locations;
        // console.log(data);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            const cleared = {
                style: '',
                fabric: '',
                color: '',
                location: '',
            };
            this.setState(cleared);
        }
    }
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({style: value})
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
            console.log(data.locations)
        }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Update hat</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                    <input value={this.state.style} onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                    <label htmlFor="style">Style</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="mb-3">
                  <select onChange={this.handleLocationChange} value={this.state.location} required name="Location" id="location" className="form-select">
                    
                    <option value="">Choose a Location</option>
                    {this.state.locations.map(state => {
                        return (
                        <option key={state.href} value={state.href}>
                            {state.closet_name} section number: {state.section_number} / {state.shelf_number}
                        </option>
                        );
                    })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Update</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}
export default HatUpdateForm;