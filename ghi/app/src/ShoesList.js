import React from 'react';
import { Link, NavLink } from 'react-router-dom';


function ShoeColumn(props) {
    return (
      <div className="col">
        {props.list.map(shoe => {
          return (
            <div key={shoe.href} className="card mb-3 shadow">
              <img src={shoe.picture_url} className="card-img-top " />
              <div className="card-body">
                <h5 className="card-title">{shoe.model_name}</h5>
                <ul>
                    <li className="card-text">
                        Color: {shoe.color}
                    </li>
                    <li className='card-text'>
                        Manufactor: {shoe.manufactor}
                    </li>
                </ul>
                <button className='btn btn-primary' onClick={() => this.delete(shoe.id)}>Delete shoes</button>
              </div>
                <div className='card-footer'>
                  <h5 className='card-title'>Bin:</h5>
                  <p className='car-text'>{shoe.bin.closet_name}</p>
                </div>
            </div>
          );
        })}
      </div>
    );
  }
  


class ShoesList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        shoeColumns: [[], [], []],
      };
    }

    
  
    async componentDidMount() {
      const url = 'http://localhost:8080/api/shoes/';
  
      try {
        const response = await fetch(url);
        if (response.ok) {
          // Get the list of conferences
          const data = await response.json();
          //console.log(data);
  
          // Create a list of for all the requests and
          // add all of the requests to it
          const requests = [];
          for (let shoe of data.shoes) {
            const detailUrl = `http://localhost:8080${shoe.href}`;
            requests.push(fetch(detailUrl));
          }
          //console.log(requests);
          // Wait for all of the requests to finish
          // simultaneously
          const responses = await Promise.all(requests);
          //console.log(responses);
          // Set up the "columns" to put the conference
          // information into
          const shoeColumns = [[], [], []];
  
          // Loop over the conference detail responses and add
          // each to to the proper "column" if the response is
          // ok
          let i = 0;
          for (const shoeResponse of responses) {
            if (shoeResponse.ok) {
              const details = await shoeResponse.json();
              //console.log(details);
              shoeColumns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(shoeResponse);
            }
          }
  
          // Set the state to the new list of three lists of
          // conferences
          this.setState({shoeColumns: shoeColumns});
          console.log(shoeColumns)
        }
      } catch (e) {
        console.error(e);
      }
    }
  
    render() {
      return (
        <>
            <h2>Shoes list</h2>
            <div className="row">
              {this.state.shoeColumns.map((shoeList, index) => {
                return (
                  <ShoeColumn key={index} list={shoeList} />
                );
              })}
            </div>
        </>
      );
    }
  }
  
  export default ShoesList;
  