import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesCreateForm from './ShoesCreateForm'
import ShoesList from './ShoesList';
<<<<<<< HEAD
import ShoesUpdateForm from './ShoesUpdateForm';
import HatForm from './HatForm';
import HatList from './HatList';
import HatUpdateForm from './HatUpdateForm';
=======
>>>>>>> 311198226227aac2b74d4a8efc08b819a1545018

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="my-5 container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='shoes' element={<ShoesList />} />
          <Route path='shoes/new' element={<ShoesCreateForm />} />
          <Route path="hats/new" element={<HatForm />} />
          <Route path="hats" element={<HatList/>} />
          <Route path='hats/update' element={<HatUpdateForm/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
