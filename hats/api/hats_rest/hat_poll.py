import json
import requests

from .models import LocationVO

def get_locations():
    response = requests.get("http://localhost:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
        )
    