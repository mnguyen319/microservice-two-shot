import json
import requests
import os
from .keys import PEXELS_API_KEY


def get_photo(style, fabric, color):
    url = "https://api.pexels.com/v1/search" 
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"hat{style}, {fabric}, {color}" 
    }
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}