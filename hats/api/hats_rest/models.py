from django.db import models
from django.urls import reverse 


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.closet_name

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(LocationVO, related_name="location", on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.style

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})

