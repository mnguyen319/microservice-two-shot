
from django.views.decorators.http import require_http_methods

from .acls import get_photo

from .models import Hat, LocationVO

from django.http import JsonResponse

from common.json import ModelEncoder, QuerySetEncoder, DateEncoder

import json

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style", "color", "fabric", "picture_url",]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style", "color", "picture_url", "location"]
    encoders = {"location": LocationVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id == None:
            hats = Hat.objects.all()
        else:
            hats = Hat.objects.filter(location=location_vo_id)
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else: 
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid hat id"}, status=400)
        photo= get_photo(content["style"], content["fabric"], content["color"])
        content.update(photo) 
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat, encoder=HatDetailEncoder, safe=False)
        

            
@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats, encoder= HatDetailEncoder, safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else: 
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )

